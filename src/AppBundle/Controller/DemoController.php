<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Demo;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;




class DemoController extends Controller
{
    /**
     * @Route("/", name="demo")
     */
    public function landingAction(Request $request){
        $adds = new Demo;

        $forms = $this->CreateFormBuilder($adds)
            ->add('name', TextType::class, array('attr' => array('class' => 'form-control', 'style' => 'margin-bottom:15px')))
            ->add('email', TextType::class, array('attr' => array('class' => 'form-control', 'style' => 'margin-bottom:15px')))
            ->add('gender', ChoiceType::class, array('choices' => array('Male' => 'Male','Female' => 'Female'),'attr' => array('class' => 'form-control', 'style' => 'margin-bottom:15px')))
            ->add('description', TextareaType::class, array('attr' => array('class' => 'form-control', 'style' => 'margin-bottom:15px')))
            ->add('password', PasswordType::class, array('attr' => array('class' => 'form-control', 'style' => 'margin-bottom:15px')))
            ->add('save', SubmitType::class, array('label' => 'Save','attr' => array('class' => 'btn btn-primary', 'style' => 'margin-bottom:15px')))
            ->getForm();

        $forms->handleRequest($request);

        if($forms->isSubmitted() && $forms->isValid()){
            //die('SUBMITTED'); //die for just testing
            //get the data from the form
            $name = $forms['name']->getData();
            $email = $forms['email']->getData();
            $gender = $forms['gender']->getData();
            $description = $forms['description']->getData();
            $password = $forms['password']->getData();

            $adds->setName($name);
            $adds->setEmail($email);
            $adds->setGender($gender);
            $adds->setDescription($description);
            $adds->setPassword($password);

            $em = $this->getDoctrine()->getManager();
            $em->persist($adds);
            $em->flush();

            $this->addFlash(        //note
                'notice',
                'Data Added'
            );

            return $this->redirectToRoute('demo_list'); //redirect to the 'demo_list' route

        }

        return $this->render('demo/index.html.twig',array(
            'forms' => $forms->createView() //pass here the create view method
        ));

        // return $this->render('demo/index.html.twig');
    }

    /**
     * @Route("/demo/form", name="demo_form")
     */
    public function formAction(Request $request){
        $add = new Demo;

        $form = $this->CreateFormBuilder($add)
            ->add('name', TextType::class, array('attr' => array('class' => 'form-control', 'style' => 'margin-bottom:15px')))
            ->add('email', TextType::class, array('attr' => array('class' => 'form-control', 'style' => 'margin-bottom:15px')))
            ->add('gender', ChoiceType::class, array('choices' => array('Male' => 'Male','Female' => 'Female'),'attr' => array('class' => 'form-control', 'style' => 'margin-bottom:15px')))
            ->add('description', TextareaType::class, array('attr' => array('class' => 'form-control', 'style' => 'margin-bottom:15px')))
            ->add('password', PasswordType::class, array('attr' => array('class' => 'form-control', 'style' => 'margin-bottom:15px')))
            ->add('save', SubmitType::class, array('label' => 'Save','attr' => array('class' => 'btn btn-primary', 'style' => 'margin-bottom:15px')))
            ->getForm();

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            //die('SUBMITTED'); //die for just testing
            //get the data from the form
            $name = $form['name']->getData();
            $email = $form['email']->getData();
            $gender = $form['gender']->getData();
            $description = $form['description']->getData();
            $password = $form['password']->getData();

            $add->setName($name);
            $add->setEmail($email);
            $add->setGender($gender);
            $add->setDescription($description);
            $add->setPassword($password);

            $em = $this->getDoctrine()->getManager();
            $em->persist($add);
            $em->flush();

            $this->addFlash(        //note
                'notice',
                'Data Added'
            );

            return $this->redirectToRoute('demo_list'); //redirect to the 'demo_list' route
        }

        return $this->render('demo/form.html.twig',array(
            'form' => $form->createView(), 'forms' => $form->createView() //pass here the create view method
        ));
    }

    /**
     * @Route("/demo/list", name="demo_list")
     */
    public function listAction(Request $request){
        $demolist = $this->getDoctrine()
            ->getRepository('AppBundle:Demo')
            ->findAll();
        // return $this->render('demo/list.html.twig', array(
        //     'lists' => $demolist
        // ));

        $adds = new Demo;

        $forms = $this->CreateFormBuilder($adds)
            ->add('name', TextType::class, array('attr' => array('class' => 'form-control', 'style' => 'margin-bottom:15px')))
            ->add('email', TextType::class, array('attr' => array('class' => 'form-control', 'style' => 'margin-bottom:15px')))
            ->add('gender', ChoiceType::class, array('choices' => array('Male' => 'Male','Female' => 'Female'),'attr' => array('class' => 'form-control', 'style' => 'margin-bottom:15px')))
            ->add('description', TextareaType::class, array('attr' => array('class' => 'form-control', 'style' => 'margin-bottom:15px')))
            ->add('password', PasswordType::class, array('attr' => array('class' => 'form-control', 'style' => 'margin-bottom:15px')))
            ->add('save', SubmitType::class, array('label' => 'Save','attr' => array('class' => 'btn btn-primary', 'style' => 'margin-bottom:15px')))
            ->getForm();

        $forms->handleRequest($request);

        if($forms->isSubmitted() && $forms->isValid()){
            //die('SUBMITTED'); //die for just testing
            //get the data from the form
            $name = $forms['name']->getData();
            $email = $forms['email']->getData();
            $gender = $forms['gender']->getData();
            $description = $forms['description']->getData();
            $password = $forms['password']->getData();

            $adds->setName($name);
            $adds->setEmail($email);
            $adds->setGender($gender);
            $adds->setDescription($description);
            $adds->setPassword($password);

            $em = $this->getDoctrine()->getManager();
            $em->persist($adds);
            $em->flush();

            $this->addFlash(        //note
                'notice',
                'Data Added'
            );

            return $this->redirectToRoute('demo_list'); //redirect to the 'demo_list' route
        }

        return $this->render('demo/list.html.twig',array(
            'forms' => $forms->createView(), 'lists' => $demolist, //pass here the create view method
        ));
    }

    /**
     * @Route("/demo/edit/{id}", name="demo_edit")
     */
    public function editAction($id, Request $request){
        $demolists = $this->getDoctrine()
            ->getRepository('AppBundle:Demo')
            ->find($id);

        //setters for the form
        $demolists->setName($demolists->getName());
        $demolists->setEmail($demolists->getEmail());
        $demolists->setGender($demolists->getGender());
        $demolists->setDescription($demolists->getDescription());
        $demolists->setPassword($demolists->getPassword());

        $form = $this->CreateFormBuilder($demolists)
            ->add('name', TextType::class, array('attr' => array('class' => 'form-control', 'style' => 'margin-bottom:15px')))
            ->add('email', TextType::class, array('attr' => array('class' => 'form-control', 'style' => 'margin-bottom:15px')))
            ->add('gender', ChoiceType::class, array('choices' => array('Male' => 'Male','Female' => 'Female'),'attr' => array('class' => 'form-control', 'style' => 'margin-bottom:15px')))
            ->add('description', TextareaType::class, array('attr' => array('class' => 'form-control', 'style' => 'margin-bottom:15px')))
            ->add('password', PasswordType::class, array('attr' => array('class' => 'form-control', 'style' => 'margin-bottom:15px')))
            ->add('save', SubmitType::class, array('label' => 'Update','attr' => array('class' => 'btn btn-primary', 'style' => 'margin-bottom:15px')))
            ->getForm();

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            //die('SUBMITTED'); //testing
            //get the data from the form
            $name = $form['name']->getData();
            $email = $form['email']->getData();
            $gender = $form['gender']->getData();
            $description = $form['description']->getData();
            $password = $form['password']->getData();

            $em = $this->getDoctrine()->getManager();
            $demolists = $em->getRepository('AppBundle:Demo')->find($id);

            $demolists->setName($name);
            $demolists->setEmail($email);
            $demolists->setGender($gender);
            $demolists->setDescription($description);
            $demolists->setPassword($password);

            $em->flush();

            $this->addFlash(        //note
                'notice',
                'Data Updated'
            );

            return $this->redirectToRoute('demo_list'); //redirect to the 'demo_list' route
        }

        return $this->render('demo/edit.html.twig', array(
            'list' => $demolists,
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/demo/view/{id}", name="demo_view")
     */
    public function viewAction($id){
        $demolists = $this->getDoctrine()
            ->getRepository('AppBundle:Demo')
            ->find($id);
        return $this->render('demo/view.html.twig', array(
            'list' => $demolists
        ));
    }

    /**
     * @Route("/demo/delete/{id}", name="demo_delete")
     */
    public function deleteAction($id){
        $em = $this->getDoctrine()->getManager();
        $demolists = $em->getRepository('AppBundle:Demo')->find($id);

        $em->remove($demolists);
        $em->flush();

        $this->addFlash(        //note
                'notice',
                'Data Deleted'
            );

        return $this->redirectToRoute('demo_list');
    }

    /**
     * @Route("demo/modal", name="demo_modal")
     */
    public function modalAction(Request $request){
        $add = new Demo;

        $form = $this->CreateFormBuilder($add)
            ->add('name', TextType::class, array('attr' => array('class' => 'form-control', 'style' => 'margin-bottom:15px')))
            ->add('email', TextType::class, array('attr' => array('class' => 'form-control', 'style' => 'margin-bottom:15px')))
            ->add('gender', ChoiceType::class, array('choices' => array('Male' => 'Male','Female' => 'Female'),'attr' => array('class' => 'form-control', 'style' => 'margin-bottom:15px')))
            ->add('description', TextareaType::class, array('attr' => array('class' => 'form-control', 'style' => 'margin-bottom:15px')))
            ->add('password', PasswordType::class, array('attr' => array('class' => 'form-control', 'style' => 'margin-bottom:15px')))
            ->add('save', SubmitType::class, array('label' => 'Save','attr' => array('class' => 'btn btn-primary', 'style' => 'margin-bottom:15px')))
            ->getForm();

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            //die('SUBMITTED'); //die for just testing
            //get the data from the form
            $name = $form['name']->getData();
            $email = $form['email']->getData();
            $gender = $form['gender']->getData();
            $description = $form['description']->getData();
            $password = $form['password']->getData();

            $add->setName($name);
            $add->setEmail($email);
            $add->setGender($gender);
            $add->setDescription($description);
            $add->setPassword($password);

            $em = $this->getDoctrine()->getManager();
            $em->persist($add);
            $em->flush();

            $this->addFlash(        //note
                'notice',
                'Data Added'
            );

            return $this->redirectToRoute('demo_list'); //redirect to the 'demo_list' route
        }

        return $this->render('demo/modal.html.twig',array(
            'form' => $form->createView() //pass here the create view method
        ));
    }

    /**
     * @Route("/home", name="homepage")
     */
    public function homeAction(Request $request){
        $demolist = $this->getDoctrine()
            ->getRepository('AppBundle:Demo')
            ->findAll();
        // return $this->render('demo/list.html.twig', array(
        //     'lists' => $demolist
        // ));

        $adds = new Demo;

        $forms = $this->CreateFormBuilder($adds)
            ->add('name', TextType::class, array('attr' => array('class' => 'form-control', 'style' => 'margin-bottom:15px')))
            ->add('email', TextType::class, array('attr' => array('class' => 'form-control', 'style' => 'margin-bottom:15px')))
            ->add('gender', ChoiceType::class, array('choices' => array('Male' => 'Male','Female' => 'Female'),'attr' => array('class' => 'form-control', 'style' => 'margin-bottom:15px')))
            ->add('description', TextareaType::class, array('attr' => array('class' => 'form-control', 'style' => 'margin-bottom:15px')))
            ->add('password', PasswordType::class, array('attr' => array('class' => 'form-control', 'style' => 'margin-bottom:15px')))
            ->add('save', SubmitType::class, array('label' => 'Save','attr' => array('class' => 'btn btn-primary', 'style' => 'margin-bottom:15px')))
            ->getForm();

        $forms->handleRequest($request);

        if($forms->isSubmitted() && $forms->isValid()){
            //die('SUBMITTED'); //die for just testing
            //get the data from the form
            $name = $forms['name']->getData();
            $email = $forms['email']->getData();
            $gender = $forms['gender']->getData();
            $description = $forms['description']->getData();
            $password = $forms['password']->getData();

            $adds->setName($name);
            $adds->setEmail($email);
            $adds->setGender($gender);
            $adds->setDescription($description);
            $adds->setPassword($password);

            $em = $this->getDoctrine()->getManager();
            $em->persist($adds);
            $em->flush();

            $this->addFlash(        //note
                'notice',
                'Data Added'
            );

            return $this->redirectToRoute('homepage'); //redirect to the 'demo_list' route
        }

        return $this->render('demo/home.html.twig',array(
            'forms' => $forms->createView(), 'form' => $forms->createView(), 'lists' => $demolist, //pass here the create view method
        ));
    }

    /**
     * @Route("/demo/fun/{username}", name="demo_fun", defaults={ "username": "mrzenu212"})
     */
    public function funAction(Request $request, $username){

        $client = new \GuzzleHttp\Client();
        $response = $client->request('GET', 'https://api.github.com/users/' . $username);

        $data = json_decode($response->getBody()->getContents(), true);

        return $this->render('demo/fun.html.twig', [
            'img_url' => $data['avatar_url'],
            'name' => $data['name'],
            'company' => $data['company'],
            'url' => $data['html_url'],
            'followers' => $data['followers'],
            'following' => $data['following'],
            'joined' => (new \DateTime($data['created_at']))->format('d m Y')
        ]);
    }

}
